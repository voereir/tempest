from tempest import clients
from tempest import config
from oslo_concurrency import processutils
from oslo_log import log as logging
from tempest.common import credentials_factory as common_creds
from tempest.scenario import manager
from tempest_lib.common.utils import data_utils
from tempest.scenario import utils as test_utils

CONF = config.CONF

LOG = logging.getLogger(__name__)

# Using 2M hugepages
HUGEPAGE_SIZE = 2048

#RAM Size of Flavor in MB
RAM_SIZE = 64

def _get_number_of_hugepages(rmt_client):
    # original command:
    #    "cat /sys/kernel/mm/hugepages/hugepages-${size}kB/"
    cmd = "cat /sys/kernel/mm/hugepages/hugepages-{}kB/nr_hugepages".format(HUGEPAGE_SIZE)
    pages = int(rmt_client.exec_command(cmd)
    LOG.debug("print page results.")
    LOG.debug(pages)
    return pages

def _get_hugepage_size(rmt_client):
    # original command:
    #    "cat /sys/kernel/mm/hugepages/hugepages-${size}kB/"
    pagesize_str = rmt_client.exec_command("grep Hugepagesize /proc/meminfo")
    LOG.debug(rmt_client.exec_command("grep HugePages /proc/meminfo"))
    pagesize_str = pagesize_str.split(":")[1]
    pagesize = int(pagesize_str.strip().split(" ")[0])
    return pagesize


class TestHugepages(manager.ScenarioTest):
    run_ssh = True
    disk_config = 'AUTO'

    @classmethod
    def setup_credentials(cls):
        super(TestHugepages, cls).setup_credentials()
        cls.manager = clients.Manager(
            credentials=common_creds.get_configured_credentials(
                'identity_admin', fill_in=False))

    def setUp(self):
        super(TestHugepages, self).setUp()
        self.image_utils = test_utils.ImageUtils(self.manager)
        if not hasattr(self, 'image_ref'):
            self.image_ref = CONF.compute.image_ref
        self.name = data_utils.rand_name('server')
        self.client = self.servers_client
        self.keypair = self.create_keypair()
        self.ssh_user = "cirros"
        
    def create_flavor_with_extra_specs(self, name='hugepages_flavor', count=1):
        flavor_with_hugepages_name = data_utils.rand_name(name)
        flavor_with_hugepages_id = data_utils.rand_int_id(start=1000)
        vcpus = 1
        disk = 0

        # set numa pagesize
        extra_specs = {"hw:mem_page_size": str(HUGEPAGE_SIZE)}
        # Create a flavor with extra specs
        resp = (self.flavors_client.
                create_flavor(name=flavor_with_hugepages_name,
                              ram=RAM_SIZE, vcpus=vcpus, disk=disk,
                              id=flavor_with_hugepages_id))
        self.flavors_client.set_flavor_extra_spec(flavor_with_hugepages_id,
                                                  **extra_specs)
        self.addCleanup(self.flavor_clean_up, flavor_with_hugepages_id)
        self.assertEqual(200, resp.response.status)

        return flavor_with_hugepages_id

    def flavor_clean_up(self, flavor_id):
        resp = self.flavors_client.delete_flavor(flavor_id)
        self.assertEqual(resp.response.status, 202)
        self.flavors_client.wait_for_resource_deletion(flavor_id)

    def verify_ssh(self):
        # Obtain a floating IP
        #floating_ip = self.create_floating_ip(self.instance)
        floating_ip = self.compute_floating_ips_client.create_floating_ip()[
            'floating_ip']
        self.addCleanup(self.delete_wrapper,
                        self.compute_floating_ips_client.delete_floating_ip,
                        floating_ip['id'])
        # Attach a floating IP
        self.compute_floating_ips_client.associate_floating_ip_to_server(
            floating_ip['ip'], self.instance['id'])
        # Check ssh
        return self.get_remote_client(
            server_or_ip=floating_ip['ip'],
            username=self.ssh_user,
            private_key=self.keypair['private_key'])
    
    def test_hugepage_backed_instance(self):
        # Calc expected hugepages
        # flavor memory/hugepage_size, rounded up
        # create instance with hugepages flavor

        flavor_id = self.create_flavor_with_extra_specs("hugepages_flavor")

        self.instance = self.create_server(wait_until="ACTIVE",
                        image=self.image_ref, flavor=flavor_id, 
                        key_name=self.keypair['name'])
        self.remote_client = self.verify_ssh()
        
        required_hugepages = RAM_SIZE / (HUGEPAGE_SIZE / 1024.)  # ram/hugepages_size
        #expected_hugepages = int(hugepages_init - required_hugepages)
        actual_hugepage_size_in_vm = _get_hugepage_size(self.remote_client)
        actual_hugepages_in_vm = _get_number_of_hugepages(self.remote_client) 
        #self.assertEqual(required_hugepages, actual_hugepages_in_vm)
        self.assertEqual(HUGEPAGE_SIZE, actual_hugepage_size_in_vm)
